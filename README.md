# LoanDolphin Test #
 
  
This project was built using Angular and Bootstrap. Gulp is used to compile into "/build" folder.  
By compiling, all javascript files (but vendors) are concatenated in just one (loandolphin.js).
Debug param can be passed to allow logs on browser console.  
Its source also can be tested with Jshint  


> Note  
> Sources are in "/code" folder.

Installing

```bash
# Install dependencies
npm install
```

Building

```bash
# Generate a new build
gulp build [--debug]

```

Options:  
* debug: enable logs

> Note  
> It builds on "/build" folder.

Running

```bash
# Start node server
node server.js

```

Analysing code

```bash
# jshint
jshint . --exclude ./node_modules
```