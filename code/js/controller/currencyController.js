/*jshint sub:true*/
Module.controller('CurrencyController', [
	'$scope',
	'$rootScope',
	'ConfigService',
	'UtilsService',
	'LogService',
	'ModelService',
	'ConverterFactory',
	function(
		$scope,
		$rootScope,
		Config,
		Utils,
		Log,
		Model,
		Converter
	){
	'use strict';
	
	var MODULE_NAME = 'CurrencyController';
	
	//make it visible from view
	$scope.config = Config;
	$scope.model = Model;	
	
	/**
	* get rates from Fixer.io services
	*/
	$scope.convert = function() {			
		
		$scope.openDropbox('from', false);
		$scope.openDropbox('to', false);
		
		//validate if both comboboxes are filled
		if((!$scope.combos['from'].selected || !$scope.combos['to'].selected) ||
			($scope.combos['from'].selected.code == $scope.combos['to'].selected.code)) {
			return;
		}
		
		var amount = $('#amount').val();
		var from = $scope.combos['from'].selected;
		var to = $scope.combos['to'].selected;
		
		Converter.convert(Number(amount), {symbols:to.code, base:from.code}).then(function (data) {
			$scope.currentConversion = {from:from,to:to,result:data,amount:amount};
		});
						
		refreshRelevants();
		refreshChart();
	};
	
	//constants to control page loader
	var RELEVANTS_LOADING = 'RELEVANTS_LOADING';
	var CHART_LOADING = 'CHART_LOADING';
	var MAIN_LOADING = 'MAIN_LOADING';
	
	function manageLoading(key, add) {
		Log.info('manageLoading('+key+', '+add+')', MODULE_NAME);
		$scope.loading = $scope.loading || [];
		if(add) {
			$scope.loading.push(key);
		} else {
			$scope.loading.splice( isLoading(key), 1 );
		}
		Log.object($scope.loading);
	}
	
	/**
	* verify if passed key is current loading
	*/
	function isLoading(key) {
		for(var i in $scope.loading) {
			if($scope.loading[i] == key) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Get user current country information by the
	 * country and location service
	 */
	function revealCurrentCountryInformations() {
			
		var locationCountry = Model.getLocation().country;
		$.each(Model.getCountries(), function(index, value) {
			if(value.name == locationCountry) {
				Model.currentCountry = value;
				return;
			}
		});
		//in case not find current country return configures as a default country
		return Model.getCountries()[Config.DEFAULT_COUNTRY_POSITION];
	}
	
	$scope.combos = {'from':{},'to':{}};
	
	$scope.setCurrency = function(currency, input) {
		if(!currency) {
			delete $scope.combos[input].selected;
			return;
		}
		$scope.combos[input].selected = currency;
		$scope.combos[input].selected.partial = $scope.combos[input].selected.partial || {};
		delete $scope.combos[input].selected.partial[input];
		refreshDescriptions();
		$scope.openDropbox(input, false);
	};
	
	/**
	* get suggestion to autocomplete combobox
	* see Utils.searchCurrencyByText()
	*/
	$scope.searchCurrencyByText = function(event, input) {
		$scope.combos[input].selected = Utils.searchCurrencyByText(input, Model, event);
	};
	
	$scope.getCurrencyByCode = function(input) {
		$scope.setCurrency(Utils.getCurrencyByCode($('#'+input).val().substring(0,3), Model), input);
	};	
	
	/**
	* refresh combobxes text
	*/
	function refreshDescriptions() {
		if($scope.combos['to'].selected) {
			$('#to').val($scope.combos['to'].selected.code + ' - ' + $scope.combos['to'].selected.name);
		}
		if($scope.combos['from'].selected) {
			$('#from').val($scope.combos['from'].selected.code + ' - ' + $scope.combos['from'].selected.name);
		}
	}
	
	/**
	* show/hide dropbox options
	*/
	$scope.openDropbox = function(input, action) {
		$scope.combos[input=='from'?'to':'from'].opened = false;
		$scope.combos[input].opened = action!==undefined?action:!$scope.combos[input].opened;
	};
	
	$scope.invertCurrencies = function() {
		//validate if both comboboxes are filled
		if((!$scope.combos['from'].selected || !$scope.combos['to'].selected) ||
			($scope.combos['from'].selected.code == $scope.combos['to'].selected.code)) {
			return;
		}
		var to = $scope.combos['to'];
		var from = $scope.combos['from'];
		$scope.combos['from'] = to;
		$scope.combos['to'] = from;
		refreshDescriptions();
	};	
	
	function populateRelevants() {
		Log.info('populateRelevants()', MODULE_NAME);
		$scope.relevantsLabels = [];
		var countries = Model.getCurrencyNames();
		for(var i=0; i<=Config.RELEVANTS_SIZE; i++) {
			$scope.relevantsLabels.push(countries[i].code);
		}
		Log.object($scope.relevantsLabels);
	}
	
	/**
	 * Append here functions that need to be executed when
	 * initialize it
	 */
	function initialize() {
	
		Log.info('initializing...', MODULE_NAME);
		revealCurrentCountryInformations();
		var currentLocaleCurrency = Model.currentCountry.currencies[0];
		if(currentLocaleCurrency) {
			currentLocaleCurrency = Utils.getCurrencyByCode(Model.currentCountry.currencies[0], Model);
			Log.object(currentLocaleCurrency);
			$scope.combos['from'].selected = currentLocaleCurrency;
		}
		populateRelevants();
		refreshRelevants();
		refreshDescriptions();
		google.charts.load('current', {'packages':['line']});
		
		getMostRelevantCurrency();
		
		//firs chat populate with current currency and most relevant what is USD or EUR
		google.charts.setOnLoadCallback(function(){		
			if($scope.mostRelevantCurrency && currentLocaleCurrency) {
				refreshChart(currentLocaleCurrency, $scope.mostRelevantCurrency);
			}
		});
	}
	
	/**
	* populate array fom relevant currencies	
	*/
	function getMostRelevantCurrency() {
		Log.info('getMostRelevantCurrency', MODULE_NAME);
		var currencies = Model.getCurrencyNames();
		$scope.mostRelevantCurrency = currencies[0].code == Model.currentCountry.currencies[0] ? currencies[1] : currencies[0];
		Log.object($scope.mostRelevantCurrency);
		return $scope.mostRelevantCurrency;
	}
	
	/**
	* calcule most importants rates by the selected currency on
	* first combobox (from)
	*/
	function refreshRelevants() {
		if(isLoading(RELEVANTS_LOADING) > -1) {
			Log.info('Already loading relevants', MODULE_NAME);
			return;
		}		
		manageLoading(RELEVANTS_LOADING, true);
		Converter.convert(1, {symbols:$scope.relevantsLabels.join(','), base:$scope.combos['from'].selected.code}).then(function (data) {
			$scope.relevants = data;
			$scope.lastFromRelevant = $scope.combos['from'].selected;
			manageLoading(RELEVANTS_LOADING, false);
		});
	}
	
	$scope.chartData = [];

	/**
	* normalize list to default required by google charts
	*/
	function refreshChart(_from, _to) {
	
		if(isLoading(CHART_LOADING) > -1) {
			Log.info('Already loading chart', MODULE_NAME);
			return;
		}
		manageLoading(CHART_LOADING, true);
		
		//get range of dates
		var dates = Utils.getChartDates();
		
		$scope.chartData = [];
		
		//if not passes as parameter take from selected on comboboxes
		var amount = $('#amount').val();
		var from = _from?_from:$scope.combos['from'].selected;
		var to = _to?_to:$scope.combos['to'].selected;
		
		$.each(dates, function(index, value) {
			Converter.convert(1, {symbols:to.code, base:from.code, date:value}).then(function (data) {
				$scope.chartData.push([value, data.rates[to.code]]);
				if($scope.chartData.length > Config.CHART_RANGE) {
					manageLoading(CHART_LOADING, false);
					manageLoading(MAIN_LOADING, false);
					Utils.drawChart($scope.chartData, from, to);
				}
			});
		});
	}
	
	/**
	* adjust size when resize browser
	*/
	$scope.redrawChart = function() {
		try {
			Utils.drawChart();
		} catch(e) {
			//prevent resize when it is not loaded yet
			Log.error(e);
		}
	};
	
	/**
	* Load services that will populate model. user current location,
	* countries informations and list of available currencies to convert
	*/
	function load() {			
	
		manageLoading(MAIN_LOADING, true);
	
		Model.getCountries();
		Model.getLocation();
		Model.getCurrencyNames();
		
		var resizeChartTimeout;
		window.onresize = function(){
		  clearTimeout(resizeChartTimeout);
		  resizeChartTimeout = setTimeout($scope.redrawChart, 500);
		};
		
	}

	/**
	* wait asynchronous requisitions to start aplication
	*/
	$rootScope.$on(Model.LOAD_EVENT, function(event, servicesLoaded){		
		
		Log.info('countries=' + servicesLoaded[Model.COUNTRIES] + ', location=' + servicesLoaded[Model.LOCATION], MODULE_NAME);
		
		var countries = Model.getCountries();
		if(servicesLoaded[Model.COUNTRIES] == Model.serviceStatus.LOADED &&
			servicesLoaded[Model.LOCATION] == Model.serviceStatus.LOADED &&
			servicesLoaded[Model.CURRENCIES] == Model.serviceStatus.LOADED) {
			//services loaded
			manageLoading(Model.COUNTRIES, false);
			manageLoading(Model.LOCATION, false);
			manageLoading(Model.CURRENCIES, false);
			
			initialize();
		}
	});
	
	load();
}]);

