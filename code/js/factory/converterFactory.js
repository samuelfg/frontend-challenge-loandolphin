Module.factory('ConverterFactory', [
	'$http',
	'UriService',
	'ConfigService',
	'UtilsService',
	'LogService',
	function(
		$http,
		Uri,
		Config,
		Utils,
		Log
	){
	'use strict';
	
	var Factory = {};	
	var MODULE_NAME = 'ConverterFactory';
	
	Factory.convert = function(amount, params) {
		Log.info('convert()', MODULE_NAME);
		return _convert(amount, params).then(function (data) {
			Log.object(data.response);
			return Utils.calculateConversion(amount, data.response);
		});
	};	
	
	
	//*** private methods ****************************************************

	
	function _convert(amount, params) {
		Log.info(Uri.conversion(params));
		return $http.get(Uri.conversion(params))
			.then(function (response) {
				return {
					success: true,
					response: response.data
				};
			})
			.catch(function (error) {		
				throw error.statusText;
			});
	}
	
	return Factory;

}]);