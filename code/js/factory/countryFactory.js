Module.factory('CountryFactory', [
	'$http',
	'UriService',
	'ConfigService',
	'UtilsService',
	'LogService',
	function(
		$http,
		Uri,
		Config,
		Utils,
		Log
	){
	'use strict';
	
	var Factory = {};	
	var MODULE_NAME = 'CountryFactory';
	
	Factory.get = function() {
		Log.info('get');
		return _get().then(function (data) {
			Log.object(data);
			return data;
		});
	};	
		
	Factory.getCurrencyNames = function() {
		Log.info('getCurrencyNames');
		return _getCurrencyNames().then(function (data) {
			Log.object(data);
			return data;
		});
	};	
		

	//*** private methods ****************************************************

	
	function _get() {
		Log.info(Uri.countries);
		return $http.get(Uri.countries)
			.then(function (response) {
				return {
					success: true,
					response: response.data
				};
			})
			.catch(function (error) {		
				throw error.statusText;
			});
	}
	
	function _getCurrencyNames() {
		Log.info(Uri.currencies);
		return $http.get(Uri.currencies)
			.then(function (response) {
				return {
					success: true,
					response: response.data
				};
			})
			.catch(function (error) {	
				throw error.statusText;
			});
	}
	
	return Factory;

}]);