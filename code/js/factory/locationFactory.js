Module.factory('LocationFactory', [
	'$http',
	'UriService',
	'ConfigService',
	'UtilsService',
	'LogService',
	function(
		$http,
		Uri,
		Config,
		Utils,
		Log
	){
	'use strict';
	
	var Factory = {};	
	var MODULE_NAME = 'LocationFactory';
	
	Factory.get = function() {
		Log.info('get()', MODULE_NAME);
		return _get().then(function (data) {
			Log.object(data);
			return data;
		});
	};	

	
	//*** private methods ****************************************************

	
	function _get() {
		Log.info(Uri.location);
		return $http.get(Uri.location)
			.then(function (response) {
				return {
					success: true,
					response: response.data
				};
			})
			.catch(function (error) {		
				throw error.statusText;
			});
	}
	
	return Factory;

}]);