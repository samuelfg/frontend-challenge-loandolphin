//service to group application constants and configurations
Module.service('ConfigService', function () {
	'use strict';
	
	//configs
	this.DEBUG_MODE = [[debug-mode]];
	this.DEFAULT_COUNTRY_POSITION = 239; //USA
	this.RELEVANTS_SIZE = 11;
	this.CHART_RANGE = 6;

});