//service to group log methods
Module.service('LogService', [
	'ConfigService',
	function (
	Config
	) {
	'use strict';
	

	this.info = function(obj, moduleName) {
		if(Config.DEBUG_MODE) {
			console.log((moduleName ? moduleName + ' | ' : '') + obj);
		}
	};
	
	this.error = function(obj, moduleName) {
		if(Config.DEBUG_MODE) {
			console.error((moduleName ? moduleName + ' | ' : '') + obj);
		}
	};
	
	this.object = function(obj) {
		if(Config.DEBUG_MODE) {
			console.log(obj);
		}
	};
	
	

}]);