//service to be used to cache models
Module.service('ModelService', [
	'$rootScope',
	'ConfigService',
	'LogService',
	'CountryFactory',
	'LocationFactory',
	function (
	$rootScope,
	Config,
	Log,
	Country,
	Location	
	) {
	'use strict';
	
	var Model = {};
	
	Model.COUNTRIES = 'countriesLoaded';
	Model.CURRENCIES = 'currenciesLoaded';
	Model.LOCATION = 'locationLoaded';
	Model.LOAD_EVENT = 'loadEvent';
	
	Model.serviceStatus = {
		PENDING : 'pending',
		LOADED : 'loaded',
		ERROR : 'error'
	};
	
	var servicesLoaded = [];
	var countries;
	var currencies;
	var location;
	
	Model.getCountries = function() {
	
		if(!countries && servicesLoaded[Model.COUNTRIES] != Model.serviceStatus.PENDING) {
			servicesLoaded[Model.COUNTRIES] = Model.serviceStatus.PENDING;
			Country.get().then(function (data) {
				countries = data.response;
				servicesLoaded[Model.COUNTRIES] = Model.serviceStatus.LOADED;				
				$rootScope.$broadcast(Model.LOAD_EVENT, servicesLoaded);
			});
		}
		return countries;
	};
	
	Model.getLocation = function() {
	
		if(!location && servicesLoaded[Model.LOCATION] != Model.serviceStatus.PENDING) {
			servicesLoaded[Model.LOCATION] = Model.serviceStatus.PENDING;
			Location.get().then(function (data) {
				location = data.response;
				servicesLoaded[Model.LOCATION] = Model.serviceStatus.LOADED;				
				$rootScope.$broadcast(Model.LOAD_EVENT, servicesLoaded);
			});		
		}
		return location;
	};
	
	Model.getCurrencyNames = function() {
	
		if(!currencies && servicesLoaded[Model.CURRENCIES] != Model.serviceStatus.PENDING) {
			servicesLoaded[Model.CURRENCIES] = Model.serviceStatus.PENDING;
			Country.getCurrencyNames().then(function (data) {
				currencies = data.response;
				servicesLoaded[Model.CURRENCIES] = Model.serviceStatus.LOADED;				
				$rootScope.$broadcast(Model.LOAD_EVENT, servicesLoaded);
			});		
		}
		return currencies;
	};
	
	return Model;
	

}]);