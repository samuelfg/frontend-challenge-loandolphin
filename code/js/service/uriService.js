//service used to group url that application need
Module.service('UriService', function () {
	'use strict';
	//get all
	this.countries = 'https://restcountries.eu/rest/v1';
	this.location = 'http://ip-api.com/json';
	this.currencies = getContextPath() + '/resources/data/currencies.json';
	this.conversion = function(params) {
		
		//to avoid nullPointerException
		params = params ? params : {};
		
		var url = 'http://api.fixer.io/'+
		(params.date?params.date:'latest')+
		(params.symbols||params.base?'?':'')+
		(params.symbols?'symbols='+params.symbols:'')+
		(params.symbols&&params.base?'&':'')+
		(params.base?'base='+params.base:'');
		return url;
	};

	/**
	 * get page context path
	 * used to concatenate on service url's
	 * @returns {string} page context path
	 */
	function getContextPath() {
		return window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));
	}


});