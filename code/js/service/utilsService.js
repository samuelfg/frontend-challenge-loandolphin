//service to group utils methods
Module.service('UtilsService', [
	'ConfigService',
	'LogService',
	function (
	Config,
	Log
	) {
	'use strict';
	
	var MODULE_NAME = 'UtilsService';
	
	/**
	* changes object retrieved from fixer.io to calculate rates according amount passed
	*/
	this.calculateConversion = function(amount, conversion) {
		for(var key in conversion.rates) {
			if(conversion.rates[key]) {
				conversion.rates[key] = conversion.rates[key] * amount;
			}
		}
		return conversion;		
	};
	
	/**
	* get suggestion to autocomplete combobox
	*/
	this.searchCurrencyByText = function(input, Model, event) {
		var value = $('#'+input).val();
		if(value.length === 0) {
			return;
		}		
		var currencies = Model.getCurrencyNames();
		for(var i in currencies) {
			var currencyString = currencies[i].code + ' - ' + currencies[i].name;
			if(currencyString.toLowerCase().startsWith(value.toLowerCase())) {
				if(!(event.keyCode == 8 ||
				event.keyCode == 46 ||
				event.keyCode == 16 ||
				event.keyCode == 17 ||
				event.keyCode == 9 ||
				(event.keyCode >= 35 && event.keyCode <= 40))) {
					$('#'+input).val(currencyString);
					$('#'+input).get(0).setSelectionRange(value.length,currencyString.length);
				}
				currencies[i].partial = currencies[i].partial || {};
				currencies[i].partial[input] = true;
				return currencies[i];
			}
		}
	};
	
	//not import Model to avoid angular circular dependecies exception
	this.getCurrencyByCode = function(code, Model) {
		Log.info('getCurrencyByCode('+code+')', MODULE_NAME);
		var currencies = Model.getCurrencyNames();
		for(var i in currencies) {
			if(currencies[i].code.toUpperCase() == code.toUpperCase()) {
				delete currencies[i].partial;
				return currencies[i];
			}
		}
		return undefined;
	};
	
	this.chartDates = [];
	this.getChartDates = function() {
		if(this.chartDates.length === 0) {
			var dates = [];
			var currDate;
			
			for(var i=0; i<=Config.CHART_RANGE; i++) {
				
				dates[i] = new Date();
				dates[i].setDate(1);
				dates[i].setMonth(dates[i].getMonth()-(i*2));
				dates[i] = this.formatTime(dates[i]);
			}
			this.chartDates = dates;
		}
		Log.object(this.chartDates);
		return this.chartDates;
	};
	
	/**
	* convert date on format required by fixer.io
	*/
	this.formatTime = function(dt) {
		return dt.getUTCFullYear() + '-' + ((dt.getUTCMonth()+1) < 10 ? '0' + (dt.getUTCMonth()+1) : (dt.getUTCMonth()+1)) + '-' + (dt.getUTCDate() < 10 ? '0' + dt.getUTCDate() : dt.getUTCDate());
	};
	
	this.chart = undefined;
	
	/**
	* singleton to return google chart instantace
	*/
	this.getChart = function() {
		if(!this.chart) {			
			this.chart = new google.charts.Line(document.getElementById('conversionChart'));
		}
		return this.chart;
	};
		
	//taked from google chart api documentation
	this.drawChart = function(chartData, from, to) {
		Log.info('DrawChart', MODULE_NAME);
		
		//it is null in case of redraw
		if(chartData) {
			this.lastData = new google.visualization.DataTable();
			this.lastData.addColumn('string', 'Date');
			this.lastData.addColumn('number', to.code);

			this.lastData.addRows(chartData);

			this.lastOptions = {
				chart: {
					title: to.code + ' ('+to.name+')' + ' rates from ' + chartData[chartData.length-1][0] + ' to ' + chartData[0][0],
					subtitle: from.code + ' ('+from.name+')'
				},
				width: '100%',
				height: 300,
				legend:{position:'none'}
			};
		}

		this.getChart().draw(this.lastData, this.lastOptions);
	};

}]);