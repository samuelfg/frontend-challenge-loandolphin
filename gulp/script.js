// jshint ignore: start
'use strict';

var config = require('./config');
var gulp = require('gulp');
var concat = require('gulp-concat');
var replace = require('gulp-replace');
var argv = require('yargs').argv;
//var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');

exports.build = function () {
	
  return gulp.src([
  'code/js/loandolphin.js',
  'code/js/*/*.js',
  '!code/js/vendors/*.js'
  ])
    .pipe(concat('loandolphin.js'))
	.pipe(replace('[[debug-mode]]', argv.debug ? true : false))
    .pipe(gulp.dest('./build/resources/'));	
};

exports.copyViews = function () {
  return gulp.src('./code/view/**')
    .pipe(gulp.dest('./build/resources/view'));
};

exports.copyScripts = function () {
  return gulp.src('./code/js/vendors/**')
    .pipe(gulp.dest('./build/resources/js/vendors'));
};

exports.copyStyles = function () {
  return gulp.src('./code/css/**')
    .pipe(gulp.dest('./build/resources/css'));
};

exports.copyData = function () {
  return gulp.src('./code/data/**')
    .pipe(gulp.dest('./build/resources/data'));
};

exports.copyImages = function () {
  return gulp.src('./code/img/**')
    .pipe(gulp.dest('./build/resources/img'));
};

exports.copyIndex = function () {
  return gulp.src('./code/index.html')
    .pipe(gulp.dest('./build'));
};
exports.compileScss = function () {
  return gulp.src('./code/css/*.scss')
   .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
   .pipe(gulp.dest('./build/resources/css'));
};

