// jshint ignore: start
'use strict';

var misc = require('./gulp/misc.js');
var build = require('./gulp/build.js');
var script = require('./gulp/script.js');
var yargs = require('yargs');
var runSequence = require('run-sequence');

var argv = yargs.argv;

var gulp = require('gulp');

gulp.task('build', ['clean'], build);

gulp.task('clean', build.clean);

gulp.task('watch', ['build'], misc.watch);
